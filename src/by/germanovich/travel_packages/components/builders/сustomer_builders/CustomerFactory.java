package by.germanovich.travel_packages.components.builders.сustomer_builders;

import by.germanovich.travel_packages.components.customer.BusinessCustomer;
import by.germanovich.travel_packages.components.customer.ResidentialCustomer;

interface CustomerFactory {

    BusinessCustomer createBusinessCustomer();

    ResidentialCustomer createResidentialCustomer();
}
