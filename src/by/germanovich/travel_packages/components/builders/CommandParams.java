package by.germanovich.travel_packages.components.builders;

import by.germanovich.travel_packages.components.agency.Agency;
import by.germanovich.travel_packages.components.customer.Customer;
import by.germanovich.travel_packages.components.tour.Tour;
import by.germanovich.travel_packages.enums.variety.Food;
import by.germanovich.travel_packages.enums.variety.Purpose;
import by.germanovich.travel_packages.enums.variety.Transport;
import by.germanovich.travel_packages.utils.Collection;
import by.germanovich.travel_packages.utils.GetData;

import java.util.List;

public class CommandParams {

    private String name;

    private String surname;
    private int card;
    private int coupon;

    private String siteName;
    private String location;

    private int discount;
    private String nameHotel;
    private int costHotel;

    private String country;
    private Purpose purpose;
    private Transport transport;
    private Food food;
    private int duration;
    private int price;

    private Customer customer;
    private Agency agency;
    private Tour tour;
    int numberAgency;

    public int getCard() {
        return card;
    }

    public void setCard(int card) {
        this.card = card;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getCoupon() {
        return coupon;
    }

    public void setCoupon(int coupon) {
        this.coupon = coupon;
    }

    public CommandParams addCard() {
        this.setCard(GetData.addCardNumber());
        return this;
    }

    public CommandParams addSiteName() {
        this.setSiteName(GetData.addSiteName());
        return this;
    }

    public CommandParams addLocation() {
        this.setLocation(GetData.addLocation());
        return this;
    }

    public CommandParams addDiscount() {
        this.setDiscount(GetData.addDiscount());
        return this;
    }

    public CommandParams addNameHotel() {
        this.setNameHotel(GetData.addNameHotel());
        return this;
    }

    public CommandParams addCostHotel() {
        this.setCostHotel(GetData.addCostHotel());
        return this;
    }

    public CommandParams addCustomer(Collection collectionData) {
        this.setCustomer(collectionData.getCustomer().get(GetData.addNumberCustomer(collectionData)));
        return this;
    }

    public CommandParams addTour(Collection collectionData) {
        this.agency = collectionData.getAgency().get(numberAgency);

        agency.addClient(customer);
        this.setTour(agency.getTour().get(GetData.addNumberTour(collectionData, numberAgency)));
        return this;
    }

    public CommandParams addAgency(Collection collectionData) {
        addNumberAgency(collectionData);
        this.setAgency(collectionData.getAgency().get(numberAgency));
        return this;
    }

    public CommandParams addNumberAgency(Collection collectionData) {
        setNumberAgency(GetData.addNumberAgency(collectionData));
        return this;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public String getNameHotel() {
        return nameHotel;
    }

    public void setNameHotel(String nameHotel) {
        this.nameHotel = nameHotel;
    }

    public int getCostHotel() {
        return costHotel;
    }

    public void setCostHotel(int costHotel) {
        this.costHotel = costHotel;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Purpose getPurpose() {
        return purpose;
    }

    public void setPurpose(Purpose purpose) {
        this.purpose = purpose;
    }

    public Transport getTransport() {
        return transport;
    }

    public void setTransport(Transport transport) {
        this.transport = transport;
    }

    public Food getFood() {
        return food;
    }

    public void setFood(Food food) {
        this.food = food;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Agency getAgency() {
        return agency;
    }

    public void setAgency(Agency agency) {
        this.agency = agency;
    }

    public Tour getTour() {
        return tour;
    }

    public void setTour(Tour tour) {
        this.tour = tour;
    }

    public int getNumberAgency() {
        return numberAgency;
    }

    public void setNumberAgency(int numberAgency) {
        this.numberAgency = numberAgency;
    }
}
