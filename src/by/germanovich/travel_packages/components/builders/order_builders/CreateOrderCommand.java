package by.germanovich.travel_packages.components.builders.order_builders;

import by.germanovich.travel_packages.components.builders.Command;
import by.germanovich.travel_packages.components.builders.CommandParams;
import by.germanovich.travel_packages.components.order.Order;
import by.germanovich.travel_packages.utils.Collection;

public  class CreateOrderCommand implements Command {

    protected OrderFactory orderFactory;

    public CreateOrderCommand() {
        orderFactory = new OrderFactoryImpl();
    }

    @Override
    public void execute(CommandParams params, Collection collectionData) {
        Order order = createNewOrder();

        fillParams(order, params, collectionData);
        collectionData.addOrder(order, params.getNumberAgency());
    }

    protected  Order createNewOrder(){
        return orderFactory.createOrder();
    }

    protected void fillParams(Order order, CommandParams params, Collection collectionData){
        params.addAgency(collectionData);
        params.addCustomer(collectionData);
        params.addTour(collectionData);

        order.setCustomer(params.getCustomer());
        order.setTour(params.getTour());
    }
}
