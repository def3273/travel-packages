package by.germanovich.travel_packages.components.builders.order_builders;

import by.germanovich.travel_packages.components.order.Order;

interface OrderFactory {

    Order createOrder();
}
