package by.germanovich.travel_packages.components.builders.order_builders;

import by.germanovich.travel_packages.components.order.Order;

public class OrderFactoryImpl implements OrderFactory {

    public Order createOrder() {
        return new Order();
    }
}
