package by.germanovich.travel_packages.components.builders.tour_builders;

import by.germanovich.travel_packages.components.builders.Command;
import by.germanovich.travel_packages.components.builders.CommandParams;
import by.germanovich.travel_packages.components.tour.Tour;
import by.germanovich.travel_packages.utils.Collection;
import by.germanovich.travel_packages.utils.GetData;

public abstract class AbstractCreateTourCommand implements Command {

    protected TourFactory tourFactory;

    AbstractCreateTourCommand() {
        tourFactory = new TourFactoryImpl();
    }

    @Override
    public void execute(CommandParams params, Collection collectionData) {
        Tour tour = createNewTour(params);

        tour.setName(params.getName());
        tour.setPrice(params.getPrice());
        tour.setCountry(params.getCountry());
        tour.setDuration(params.getDuration());
        tour.setFood(params.getFood());
        tour.setPurpose(params.getPurpose());
        tour.setTransport(params.getTransport());

        int number = GetData.addNumberAgency(collectionData);

        fillParams(tour, params);
        collectionData.addTour(tour, number);
    }

    protected abstract Tour createNewTour(CommandParams params);

    protected abstract void fillParams(Tour tour, CommandParams params);
}
