package by.germanovich.travel_packages.components.builders.tour_builders;

import by.germanovich.travel_packages.components.builders.CommandParams;
import by.germanovich.travel_packages.components.tour.EconomyTour;
import by.germanovich.travel_packages.components.tour.Tour;

public class CreateEconomyTourCommand extends AbstractCreateTourCommand {
    @Override
    protected EconomyTour createNewTour(CommandParams params){
        return tourFactory.createEconomyTour();
    }

    protected void fillParams(Tour tour, CommandParams params){
        params.addDiscount();
        EconomyTour economyTour = ((EconomyTour)tour);
        economyTour.setDiscount(params.getDiscount());
    }
}
