package by.germanovich.travel_packages.components.builders.tour_builders;

import by.germanovich.travel_packages.components.tour.EconomyTour;
import by.germanovich.travel_packages.components.tour.EliteTour;

public class TourFactoryImpl implements TourFactory {

    public EconomyTour createEconomyTour() {
        return new EconomyTour();
    }

    public EliteTour createEliteTour() {
        return new EliteTour();
    }
}
