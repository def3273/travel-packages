package by.germanovich.travel_packages.components.builders.tour_builders;

import by.germanovich.travel_packages.components.tour.EconomyTour;
import by.germanovich.travel_packages.components.tour.EliteTour;

interface TourFactory {

    EconomyTour createEconomyTour();

    EliteTour createEliteTour();
}
