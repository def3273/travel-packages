package by.germanovich.travel_packages.components.builders.agency_builders;

import by.germanovich.travel_packages.components.agency.Agency;
import by.germanovich.travel_packages.components.builders.Command;
import by.germanovich.travel_packages.components.builders.CommandParams;
import by.germanovich.travel_packages.utils.Collection;

public abstract class AbstractCreateAgencyCommand implements Command {

    protected AgencyFactory agencyFactory;

    AbstractCreateAgencyCommand() {
        agencyFactory = new AgencyFactoryImpl();
    }

    @Override
    public void execute(CommandParams params, Collection collectionData) {
        Agency agency = createNewCustomer(params);
        agency.setName(params.getName());

        fillParams(agency, params);
        collectionData.addAgency(agency);
    }

    protected abstract Agency createNewCustomer(CommandParams params);

    protected abstract void fillParams(Agency agency, CommandParams params);
}
