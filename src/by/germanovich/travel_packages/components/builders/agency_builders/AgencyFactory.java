package by.germanovich.travel_packages.components.builders.agency_builders;

import by.germanovich.travel_packages.components.agency.LocalAgency;
import by.germanovich.travel_packages.components.agency.OnlineAgency;

interface AgencyFactory {

    LocalAgency createLocalAgency();

    OnlineAgency createOnlineAgency();
}
