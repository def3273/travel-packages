package by.germanovich.travel_packages.components.builders.agency_builders;

import by.germanovich.travel_packages.components.agency.Agency;
import by.germanovich.travel_packages.components.agency.LocalAgency;
import by.germanovich.travel_packages.components.builders.CommandParams;

public class CreateLocalAgencyCommand extends AbstractCreateAgencyCommand {
    @Override
    protected LocalAgency createNewCustomer(CommandParams params){
        return agencyFactory.createLocalAgency();
    }

    @Override
    protected void fillParams(Agency agency, CommandParams params){
        params.addLocation();
        LocalAgency localAgency = ((LocalAgency)agency);
        localAgency.setLocation(params.getLocation());
    }
}
