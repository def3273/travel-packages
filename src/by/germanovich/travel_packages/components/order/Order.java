package by.germanovich.travel_packages.components.order;

import by.germanovich.travel_packages.components.customer.Customer;
import by.germanovich.travel_packages.components.tour.Tour;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.StringJoiner;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Order", namespace = "http://www.travel-packages.com/Order", propOrder = {
        "customer",
        "tour",
        "dateCalendar",
})
public class Order implements Comparable<Order> {

    private final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy MMMM dd HH:mm:ss");

    private static int idGenerator = 1;

    @XmlAttribute(name = "ID", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    private int id;

    @XmlElement(name = "Customer", required = true)
    private Customer customer;

    @XmlElement(name = "Tour", required = true)
    private Tour tour;

    @XmlElement(name = "Calendar", required = true)
    private GregorianCalendar dateCalendar;

    public Order(Customer customer, Tour tour) {
        setCustomer(customer);
        setTour(tour);
        setDateCalendar();
        this.id = generateId();
    }

    public Order() {
        setDateCalendar();
        this.id = generateId();
    }

    public int generateId() {
        int idNext = idGenerator;
        idGenerator++;
        return idNext;
    }

    @XmlAttribute
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @XmlElement
    public Customer getCustomer() {
        return customer;
    }

    public void setTour(Tour tour) {
        this.tour = tour;
    }

    @XmlElement
    public Tour getTour() {
        return tour;
    }

    @XmlElement
    public GregorianCalendar getDate() {
        return dateCalendar;
    }

    public void setDateCalendar(GregorianCalendar dateCalendar) {
        this.dateCalendar = dateCalendar;
    }

    public void setDateCalendar() {
        dateCalendar = new GregorianCalendar();
    }

    @Override
    public int compareTo(Order order) {
        {
            return Integer.compare(this.id, order.id);
        }
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Order.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("customer=" + customer)
                .add("tour=" + tour)
                .add("dateCalendar=" + DATE_FORMAT.format(dateCalendar.getTime()))
                .toString();
    }
}
