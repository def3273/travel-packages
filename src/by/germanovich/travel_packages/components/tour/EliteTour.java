package by.germanovich.travel_packages.components.tour;

import by.germanovich.travel_packages.enums.variety.Food;
import by.germanovich.travel_packages.enums.variety.Purpose;
import by.germanovich.travel_packages.enums.variety.Transport;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.StringJoiner;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EliteTour", namespace = "http://www.travel-packages.com/EliteTour", propOrder = {
        "nameHotel",
        "costHotel"
})
public class EliteTour extends Tour {

    @XmlElement(name = "NameHotel", required = true)
    private String nameHotel;

    @XmlElement(name = "CostHotel", required = true)
    private int costHotel;

    public EliteTour(String country, String name, Purpose purpose, Transport transport, Food food, int duration,
                     int price, String nameHotel, int costHotel) {

        super(country, name, purpose, transport, food, duration, price);
        this.nameHotel = nameHotel;
        this.costHotel = costHotel;
    }

    public EliteTour() {
    }

    @XmlElement
    public String getNameHotel() {
        return nameHotel;
    }

    public void setNameHotel(String nameHotel) {
        this.nameHotel = nameHotel;
    }

    @XmlElement
    public int getCostHotel() {
        return costHotel;
    }

    public void setCostHotel(int costHotel) {
        this.costHotel = costHotel;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", EliteTour.class.getSimpleName() + "[", "]")
                .add("price=" + super.getPrice() + "$")
                .add("nameHotel='" + nameHotel + "'")
                .add("costHotel=" + costHotel + "$")
                .toString();
    }
}
