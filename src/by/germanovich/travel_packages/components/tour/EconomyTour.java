package by.germanovich.travel_packages.components.tour;

import by.germanovich.travel_packages.enums.variety.Food;
import by.germanovich.travel_packages.enums.variety.Purpose;
import by.germanovich.travel_packages.enums.variety.Transport;
import by.germanovich.travel_packages.utils.FinanceCalculation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.StringJoiner;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EconomyTour", namespace = "http://www.travel-packages.com/EconomyTour", propOrder = {
        "discount",
})
public class EconomyTour extends Tour implements FinanceCalculation {

    @XmlElement(name = "Discount", required = true)
    private int discount;

    public EconomyTour(String country, String name, Purpose purpose, Transport transport, Food food, int duration,
                       int price, int discount) {

        super(country, name, purpose, transport, food, duration, price);
        this.discount = discount;
    }

    public EconomyTour() {
    }

    @XmlElement
    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    @Override
    public int tripPrice() {
        double price = (double) getPrice();
        if (getDiscount() < 1) {
            return getPrice();
        }
        return (int) (price / 100 * getDiscount());
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", EconomyTour.class.getSimpleName() + "[", "]")
                .add("price=" + super.getPrice() + "$")
                .add("discount=" + discount + "%")
                .toString();
    }
}
