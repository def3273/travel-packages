package by.germanovich.travel_packages.components.tour;

import by.germanovich.travel_packages.enums.variety.Food;
import by.germanovich.travel_packages.enums.variety.Purpose;
import by.germanovich.travel_packages.enums.variety.Transport;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Objects;
import java.util.StringJoiner;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tour", namespace = "http://www.travel-packages.com/Tour", propOrder = {
        "country",
        "name",
        "country",
        "purpose",
        "transport",
        "food",
        "duration",
        "price"
})
public abstract class Tour implements Comparable<Tour> {

    private static int idGenerator = 1;

    @XmlAttribute(name = "ID", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    private int id;

    @XmlElement(name = "Country", required = true)
    private String country;

    @XmlElement(name = "Name", required = true)
    private String name;

    @XmlElement(name = "Purpose", required = true)
    private Purpose purpose;

    @XmlElement(name = "Transport", required = true)
    private Transport transport;

    @XmlElement(name = "Food", required = true)
    private Food food;

    @XmlElement(name = "Duration", required = true)
    private int duration;

    @XmlElement(name = "Price", required = true)
    private int price;

    public Tour(String country, String name, Purpose purpose, Transport transport, Food food, int duration, int price) {

        setCountry(country);
        setName(name);
        setPurpose(purpose);
        setTransport(transport);
        setFood(food);
        setDuration(duration);
        setPrice(price);
        this.id = generateID();
    }

    public Tour() {

        this.id = generateID();
    }

    public int generateID() {
        int idNext = idGenerator;
        idGenerator++;
        return idNext;
    }

    @XmlAttribute
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @XmlElement
    public String getCountry() {
        return country;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlElement
    public String getName() {
        return name;
    }

    public void setPurpose(Purpose purpose) {
        this.purpose = purpose;
    }

    @XmlElement
    public Purpose getPurpose() {
        return purpose;
    }

    public void setTransport(Transport transport) {
        this.transport = transport;
    }

    @XmlElement
    public Transport getTransport() {
        return transport;
    }

    public void setFood(Food food) {
        this.food = food;
    }

    @XmlElement
    public Food getFood() {
        return food;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    @XmlElement
    public int getDuration() {
        return duration;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @XmlElement
    public int getPrice() {
        return price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Tour tour = (Tour) o;
        return duration == tour.duration &&
                price == tour.price &&
                Objects.equals(country, tour.country) &&
                Objects.equals(name, tour.name) &&
                purpose == tour.purpose &&
                transport == tour.transport &&
                food == tour.food;
    }

    @Override
    public int hashCode() {
        return Objects.hash(country, name, purpose, transport, food, duration, price);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Tour.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("country='" + country + "'")
                .add("name='" + name + "'")
                .add("purpose=" + purpose)
                .add("transport=" + transport)
                .add("food=" + food)
                .add("duration=" + duration)
                .add("price=" + price)
                .toString();
    }

    @Override
    public int compareTo(Tour anotherTour) {
        return this.price - anotherTour.getPrice();
    }
}
