package by.germanovich.travel_packages.components.agency;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.StringJoiner;

@XmlType(name = "LocalAgency", namespace = "http://www.travel-packages.com/LocalAgency", propOrder = {"location"})
public class LocalAgency extends Agency {

    @XmlElement(name = "Location", required = true)
    private String location;

    public LocalAgency(String name, String location) {

        super(name);
        this.location = location;
    }

    public LocalAgency() {
    }

    @XmlElement
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", LocalAgency.class.getSimpleName() + "[", "]")
                .add("location='" + location + "'")
                .toString();
    }
}
