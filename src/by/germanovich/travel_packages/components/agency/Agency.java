package by.germanovich.travel_packages.components.agency;

import by.germanovich.travel_packages.components.order.Order;
import by.germanovich.travel_packages.components.customer.Customer;
import by.germanovich.travel_packages.components.tour.Tour;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Agency", namespace = "http://www.travel-packages.com/Agency", propOrder = {
        "name",
        "tour",
        "customer",
        "order",
})
public abstract class Agency {

    private String name;
    private List<Tour> tour;
    private List<Customer> customer;
    private List<Order> order;

    public Agency(String name) {
        this.name = name;
        tour = new ArrayList<>();
        customer = new ArrayList<>();
        order = new ArrayList<>();
    }

    public Agency() {
        tour = new ArrayList<>();
        customer = new ArrayList<>();
        order = new ArrayList<>();
    }

    @XmlElement
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlElement
    public List<Tour> getTour() {
        return tour;
    }

    @XmlElement
    public List<Customer> getCustomer() {
        return customer;
    }

    @XmlElement
    public List<Order> getOrder() {
        return order;
    }

    public void setTour(List<Tour> tour) {
        this.tour = tour;
    }

    public void setCustomer(List<Customer> customer) {
        this.customer = customer;
    }

    public void setOrder(List<Order> order) {
        this.order = order;
    }

    public void addOrder(Order order) {
        this.order.add(order);
    }

    public void addTour(Tour tour) {
        this.tour.add(tour);
    }

    public void addClient(Customer customer) {
        this.customer.add(customer);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Agency agency = (Agency) o;
        return name.equals(agency.name) &&
                tour.equals(agency.tour) &&
                customer.equals(agency.customer) &&
                order.equals(agency.order);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, tour, customer, order);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Agency.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .add("tour=" + tour)
                .add("customer=" + customer)
                .add("order=" + order)
                .toString();
    }
}
