package by.germanovich.travel_packages.components.agency;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.StringJoiner;

@XmlType(name = "OnlineAgency", namespace = "http://www.travel-packages.com/OnlineAgency", propOrder = {"siteName"})
public class OnlineAgency extends Agency {

    @XmlElement(name = "siteName", required = true)
    private String siteName;

    public OnlineAgency(String name, String siteName) {

        super(name);
        this.siteName = siteName;
    }

    public OnlineAgency() {
    }

    @XmlElement
    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", OnlineAgency.class.getSimpleName() + "[", "]")
                .add("siteName='" + siteName + "'")
                .toString();
    }
}
