package by.germanovich.travel_packages.components.customer;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.StringJoiner;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResidentialCustomer", namespace = "http://www.travel-packages.com/ResidentialCustomer",
        propOrder = {"cardNumber"})
public class ResidentialCustomer extends Customer {

    @XmlElement(name = "cardNumber", required = true)
    private int cardNumber;

    public ResidentialCustomer(String name, String surname, int cardNumber) {
        super(name, surname);
        this.cardNumber = cardNumber;
    }

    public ResidentialCustomer() {
    }

    @XmlElement
    public int getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(int cardNumber) {
        this.cardNumber = cardNumber;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ResidentialCustomer.class.getSimpleName() + "[", "]")
                .add(super.getName() + " " + super.getSurname())
                .add("cardNumber=" + cardNumber)
                .toString();
    }
}
