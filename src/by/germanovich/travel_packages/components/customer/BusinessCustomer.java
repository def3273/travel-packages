package by.germanovich.travel_packages.components.customer;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.StringJoiner;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BusinessCustomer", namespace = "http://www.travel-packages.com/BusinessCustomer",
        propOrder = {"businessCardNumber"})
public class BusinessCustomer extends Customer {

    @XmlElement(name = "businessCardNumber", required = true)
    private int businessCardNumber;

    public BusinessCustomer(String name, String surname, int businessCardNumber) {
        super(name, surname);
        this.businessCardNumber = businessCardNumber;
    }

    public BusinessCustomer() {
    }

    @XmlElement
    public int getCardNumber() {
        return businessCardNumber;
    }

    public void setCardNumber(int businessCardNumber) {
        this.businessCardNumber = businessCardNumber;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", BusinessCustomer.class.getSimpleName() + "[", "]")
                .add(super.getName() + " " + super.getSurname())
                .add("businessCardNumber=" + businessCardNumber)
                .toString();
    }
}
