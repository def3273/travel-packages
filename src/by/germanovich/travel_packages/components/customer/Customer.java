package by.germanovich.travel_packages.components.customer;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.StringJoiner;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Customer", namespace = "http://www.travel-packages.com/Customer", propOrder = {
        "name",
        "surname",
})
public abstract class Customer {

    private static int idGenerator = 1;
    @XmlAttribute(name = "ID", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    private int id;
    @XmlElement(name = "Name", required = true)
    private String name;
    @XmlElement(name = "Same", required = true)
    private String surname;

    public Customer(String name, String surname) {
        setName(name);
        setSurname(surname);
        this.id = generateID();
    }

    public Customer() {
        this.id = generateID();
    }

    public int generateID() {
        int idNext = idGenerator;
        idGenerator++;
        return idNext;
    }

    @XmlAttribute
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlElement
    public String getName() {
        return name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @XmlElement
    public String getSurname() {
        return surname;
    }

    public abstract int getCardNumber();

    @Override
    public String toString() {
        return new StringJoiner(", ", Customer.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("name='" + name + "'")
                .add("surname='" + surname + "'")
                .toString();
    }
}
