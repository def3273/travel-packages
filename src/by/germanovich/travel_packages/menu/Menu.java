package by.germanovich.travel_packages.menu;

import by.germanovich.travel_packages.components.builders.CommandParams;
import by.germanovich.travel_packages.components.builders.CommandParamsBuilder;
import by.germanovich.travel_packages.components.builders.agency_builders.CreateOnlineAgencyCommand;
import by.germanovich.travel_packages.components.builders.Command;
import by.germanovich.travel_packages.components.builders.order_builders.CreateOrderCommand;
import by.germanovich.travel_packages.components.builders.tour_builders.CreateEconomyTourCommand;
import by.germanovich.travel_packages.components.builders.tour_builders.CreateEliteTourCommand;
import by.germanovich.travel_packages.components.builders.сustomer_builders.*;
import by.germanovich.travel_packages.components.order.Order;
import by.germanovich.travel_packages.enums.menu_type.*;
import by.germanovich.travel_packages.menu.actions.*;
import by.germanovich.travel_packages.utils.*;

import java.awt.*;
import java.util.HashMap;

public class Menu {


    private HashMap<CommandType, Command> сommandMap = new HashMap<>();
//    private Collection collectionDate = Collection.getInstance();
    private Collection collectionDate = new Collection();

    public Menu() {
        init();
    }

    private void init() {
        сommandMap.put(CommandType.CREATE_RESIDENTIAL_CUSTOMER, new CreateResidentialCustomerCommand());
        сommandMap.put(CommandType.CREATE_BUSINESS_CUSTOMER, new CreateBusinessCustomerCommand());
        сommandMap.put(CommandType.CREATE_ONLINE_AGENCY, new CreateOnlineAgencyCommand());
        сommandMap.put(CommandType.CREATE_LOCAL_AGENCY, new CreateOnlineAgencyCommand());
        сommandMap.put(CommandType.CREATE_ECONOMY_TOUR, new CreateEconomyTourCommand());
        сommandMap.put(CommandType.CREATE_ELITE_TOUR, new CreateEliteTourCommand());
        сommandMap.put(CommandType.CREATE_ORDER, new CreateOrderCommand());
    }

    public void processEvent(AddCustomer event) {
        //logic for keyboard and terminal interaction

        CommandType commandType = CustomerType.intToEnum();
        Command commandToExecute = сommandMap.get(commandType);
        if (commandToExecute != null) {

            CommandParams commandParams =
                    new CommandParamsBuilder()
                            .addName(event.getName())
                            .addSurname(event.getSurname())
                            .build();

            try {
                commandToExecute.execute(commandParams, collectionDate);
            } catch (Exception exception) {
                //System.out.println(exception.getErrorMessage());
            }
        }

    }

    public void processEvent(AddTour event) {

        CommandType commandType = TourType.intToEnum();
        Command commandToExecute = сommandMap.get(commandType);
        if (commandToExecute != null) {

            CommandParams commandParams =
                    new CommandParamsBuilder()
                            .addCountry(event.getCountry())
                            .addName(event.getName())
                            .addPurpose(event.getPurpose())
                            .addTransport(event.getTransport())
                            .addFood(event.getFood())
                            .addDuration(event.getDuration())
                            .addPrice(event.getPrice())
                            .build();

            try {
                commandToExecute.execute(commandParams, collectionDate);
            } catch (Exception exception) {
                //System.out.println(exception.getErrorMessage());
            }
        }
    }

    public void processEvent(AddAgency event) {

        CommandType commandType = AgencyType.intToEnum();
        Command commandToExecute = сommandMap.get(commandType);
        if (commandToExecute != null) {
            //create commandParams...

            CommandParams commandParams =
                    new CommandParamsBuilder()
                            .addName(event.getName())
                            .build();

            try {
                commandToExecute.execute(commandParams, collectionDate);
            } catch (Exception exception) {
                //System.out.println(exception.getErrorMessage());
            }
        }
    }

    public void processEvent(Order event) {

        CommandType commandType = OrderType.intToEnum();
        Command commandToExecute = сommandMap.get(commandType);
        if (commandToExecute != null) {
            //create commandParams...

            CommandParams commandParams =
                    new CommandParamsBuilder()
                            .build();

            try {
                commandToExecute.execute(commandParams, collectionDate);
            } catch (Exception exception) {
                //System.out.println(exception.getErrorMessage());
            }
        }
    }

    public void start() {

        while (true) {
            MainMenu.printMainMenu();

            switch (Keyboard.inputNumberWithZero()) {

                case Constants.ADD_TEST_DATE:
                    MenuOperations.addTestDate(collectionDate);
                    MenuOperations.addTestCustomers(collectionDate);
                    break;

                case Constants.ADD_TOUR:
                    AddTour tour = new AddTour();
                    processEvent(tour);
                    break;

                case Constants.ADD_ORDER:
                    Order order = new Order();
                    processEvent(order);
                    break;

                case Constants.ADD_AGENCY:
                    AddAgency agency = new AddAgency();
                    processEvent(agency);
                    break;

                case Constants.ADD_CUSTOMER:
                    AddCustomer customer = new AddCustomer();
                    processEvent(customer);
                    break;

                case Constants.REMOVE_AGENCY:
                    RemoveAgency.remove(collectionDate);
                    break;

                case Constants.REMOVE_CUSTOMER:
                    RemoveCustomer.remove(collectionDate);
                    break;

                case Constants.REMOVE_ORDER:
                    RemoveOrder.remove(collectionDate);
                    break;

                case Constants.REMOVE_TOUR:
                    RemoveTour.remove(collectionDate);
                    break;

                case Constants.PRINT:
                    PrintData.printCollection(collectionDate);
                    break;

                case Constants.SORT_BY_PRICE:
                    SortCollectionData.sortByPrice(collectionDate);
                    break;

                case Constants.EXIT:
                    System.out.println("Работа завершена...");
                    System.exit(0);

                case Constants.SAVE_DATA:
                    MenuOperations.saveData(collectionDate);
                    break;

                case Constants.DOWNLOAD_DATA:
                    collectionDate = MenuOperations.downloadData(collectionDate);
                    break;

                default:
                    System.out.println(Constants.INVALID_INPUT);
                    Toolkit.getDefaultToolkit().beep();
            }


        }
    }
}
