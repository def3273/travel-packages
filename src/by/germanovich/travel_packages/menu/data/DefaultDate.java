package by.germanovich.travel_packages.menu.data;

import by.germanovich.travel_packages.components.order.Order;
import by.germanovich.travel_packages.components.agency.Agency;
import by.germanovich.travel_packages.components.agency.LocalAgency;
import by.germanovich.travel_packages.components.agency.OnlineAgency;
import by.germanovich.travel_packages.components.customer.Customer;
import by.germanovich.travel_packages.components.customer.BusinessCustomer;
import by.germanovich.travel_packages.components.customer.ResidentialCustomer;
import by.germanovich.travel_packages.components.tour.EconomyTour;
import by.germanovich.travel_packages.components.tour.EliteTour;
import by.germanovich.travel_packages.components.tour.Tour;
import by.germanovich.travel_packages.enums.variety.Food;
import by.germanovich.travel_packages.enums.variety.Purpose;
import by.germanovich.travel_packages.enums.variety.Transport;

import java.util.ArrayList;
import java.util.List;

public class DefaultDate {
    public static List<Agency> giveDefaultDate(){
        List<Agency> defaultAgency = new ArrayList<>();
        Agency onlineAgency = new OnlineAgency("World", "agency.org");
        Agency localAgency = new LocalAgency("Country", "Belarus");

        Tour russia = new EconomyTour(
                "Russia",
                "Fun trip",
                Purpose.RECREATION,
                Transport.CAR,
                Food.DINNER,
                14,
                555,
                25);

        Tour egypt = new EconomyTour(
                "Egypt",
                "Beautiful world",
                Purpose.EXCURSION,
                Transport.PLANE,
                Food.ALL_INCLUSIVE,
                7,
                657,
                30);

        Tour japan = new EconomyTour(
                "Japan",
                "Recovery",
                Purpose.TREATMENT,
                Transport.PLANE,
                Food.BREAKFAST,
                7,
                700,
                5);

        Tour poland = new EliteTour(
                "Poland",
                "Sell-out",
                Purpose.SHOPPING,
                Transport.BUS,
                Food.DINNER,
                21,
                124,
                "LiteWorld",
                250);

        Tour southAfrica = new EliteTour(
                "South Africa",
                "Hot spot",
                Purpose.RECREATION,
                Transport.PLANE,
                Food.BRUNCH,
                28,
                2000,
                "HotLike",
                120);

        Tour mexico = new EliteTour(
                "Mexico",
                "Fun trip",
                Purpose.RECREATION,
                Transport.PLANE,
                Food.BREAKFAST,
                7,
                999,
                "IMexico",
                320);

        onlineAgency.addTour(russia);
        onlineAgency.addTour(egypt);
        onlineAgency.addTour(japan);

        localAgency.addTour(poland);
        localAgency.addTour(southAfrica);
        localAgency.addTour(mexico);

        Customer customer1 = new BusinessCustomer("Frey", "Brynjolf", 1258685);
        Customer customer2 = new ResidentialCustomer("German", "Brown", 14584798);

        onlineAgency.addClient(customer1);
        onlineAgency.addClient(customer2);

        onlineAgency.addOrder(new Order(customer1, japan));
        onlineAgency.addOrder(new Order(customer2, poland));

        defaultAgency.add(onlineAgency);
        defaultAgency.add(localAgency);
        return defaultAgency;
    }

    public static List<Customer> giveDefaultCustomers(){
        List<Customer> defaultCustomer = new ArrayList<>();

        Customer customer1 = new BusinessCustomer("Frey", "Brynjolf", 1258685);
        Customer customer2 = new ResidentialCustomer("German", "Brown", 14584798);

        defaultCustomer.add(customer1);
        defaultCustomer.add(customer2);
        return defaultCustomer;
    }
}
