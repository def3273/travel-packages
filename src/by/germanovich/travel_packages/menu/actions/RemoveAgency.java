package by.germanovich.travel_packages.menu.actions;

import by.germanovich.travel_packages.utils.Collection;
import by.germanovich.travel_packages.utils.Keyboard;
import by.germanovich.travel_packages.utils.PrintData;

public class RemoveAgency {
    public static void remove(Collection myCollection) {
        int numberAgency;

        do {
            PrintData.printAgency(myCollection);
            System.out.println("Введите номер агенства:");
            numberAgency = Keyboard.inputNumber() - 1;

            if (numberAgency < myCollection.getAgency().size()) {
                myCollection.getAgency().remove(numberAgency);
                break;
            } else {
                System.out.println("Агенства под таким числом нет");
            }
        } while (numberAgency >= myCollection.getAgency().size());
    }
}
