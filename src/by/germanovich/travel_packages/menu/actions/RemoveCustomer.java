package by.germanovich.travel_packages.menu.actions;

import by.germanovich.travel_packages.utils.Collection;
import by.germanovich.travel_packages.utils.Keyboard;
import by.germanovich.travel_packages.utils.PrintData;

public class RemoveCustomer {
    public static void remove(Collection myCollection) {
        int numberCustomer;

        do {
            PrintData.printCustomer(myCollection);
            System.out.println("Введите номер пользователя:");
            numberCustomer = Keyboard.inputNumber() - 1;

            if (numberCustomer < myCollection.getCustomer().size()) {

                myCollection.getCustomer().remove(numberCustomer);
                break;
            } else {

                System.out.println("Пользователя под таким числом нет");
            }
        } while (numberCustomer >= myCollection.getCustomer().size());
    }
}
