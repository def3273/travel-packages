package by.germanovich.travel_packages.menu.actions;

import by.germanovich.travel_packages.utils.Collection;
import by.germanovich.travel_packages.utils.GetData;
import by.germanovich.travel_packages.utils.Keyboard;
import by.germanovich.travel_packages.utils.PrintData;

public class RemoveOrder {
    public static void remove(Collection myCollection) {

        int numberOrder;

        int numberAgency = GetData.addNumberAgency(myCollection);

        do {
            PrintData.printOrder(myCollection, numberAgency);
            System.out.println("Введите номер заказа:");
            numberOrder = Keyboard.inputNumber() - 1;

            if (numberOrder < myCollection.getAgency().get(numberAgency).getOrder().size()) {

                myCollection.getAgency()
                        .get(numberAgency)
                        .getOrder()
                        .remove(numberOrder);
                break;
            } else {

                System.out.println("Заказа под таким числом нет");
            }

        } while (numberOrder >= myCollection.getAgency().get(numberAgency).getOrder().size());
    }
}
