package by.germanovich.travel_packages.menu.actions;

import by.germanovich.travel_packages.utils.Collection;
import by.germanovich.travel_packages.utils.GetData;
import by.germanovich.travel_packages.utils.Keyboard;
import by.germanovich.travel_packages.utils.PrintData;

public class RemoveTour {
    public static void remove(Collection myCollection) {

        int numberTour;

        int numberAgency = GetData.addNumberAgency(myCollection);

        do {
            PrintData.printTour(myCollection, numberAgency);
            System.out.println("Введите номер тура:");
            numberTour = Keyboard.inputNumber() - 1;
            if (numberTour < myCollection.getAgency().get(numberAgency).getTour().size()) {

                myCollection.getAgency()
                        .get(numberAgency)
                        .getTour()
                        .remove(numberTour);
                break;
            } else {
                System.out.println("Тура под таким числом нет");
            }

        } while (numberTour >= myCollection.getAgency().get(numberAgency).getTour().size());
    }
}
