package by.germanovich.travel_packages.utils;

public class MainMenu {

    public static void printMainMenu() {
        System.out.println(Constants.THICK_LINE);
        System.out.println("1. Добавить изначальные данные");
        System.out.println("2. Добавить тур");
        System.out.println("3. Добавить заказ");
        System.out.println("4. Добавить агенство");
        System.out.println("5. Добавить пользователя");
        System.out.println("6. Удалить агенство");
        System.out.println("7. Удалить пользователя");
        System.out.println("8. Удалить заказ");
        System.out.println("9. Удалить тур");
        System.out.println("10. Сортировать туры по цене");
        System.out.println("11. Вывести всю информацию");
        System.out.println("12. Сохранить данные");
        System.out.println("13. Загрузить данные");
        System.out.println("0. Выход");
        System.out.println(Constants.THIN_LINE);
        System.out.print("Введите команду: ");

    }
}
