package by.germanovich.travel_packages.utils;

public interface FinanceCalculation {
    int tripPrice();
}
