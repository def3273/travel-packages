package by.germanovich.travel_packages.utils;

import by.germanovich.travel_packages.components.agency.Agency;
import by.germanovich.travel_packages.components.customer.Customer;
import by.germanovich.travel_packages.menu.data.DefaultDate;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.*;
import java.util.List;

public class MenuOperations {
    public static Collection addTestDate(Collection myCollection) {

        List<Agency> files = DefaultDate.giveDefaultDate();
        myCollection.addListAgency(files);
        return myCollection;
    }

    public static Collection addTestCustomers(Collection myCollection) {
        List<Customer> files = DefaultDate.giveDefaultCustomers();
        myCollection.addListCustomer(files);
        return myCollection;
    }
    public static void saveData(Collection myCollection) {
        try {
            XMLEncoder encoder = new XMLEncoder(
                    new BufferedOutputStream(
                            new FileOutputStream("DataFile.xml")));

            encoder.writeObject(myCollection);
            encoder.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    public static Collection downloadData(Collection myCollection) {
        try {
            FileInputStream fileInputStream = new FileInputStream(new File("DataFile.xml"));
            XMLDecoder decoder = new XMLDecoder(fileInputStream);
            Collection downloadCollection = ((Collection) decoder.readObject());
            decoder.close();
            return downloadCollection;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("Не удалось загручить данные!");
        return myCollection;
    }
}
