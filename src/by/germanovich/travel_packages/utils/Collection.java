package by.germanovich.travel_packages.utils;

import by.germanovich.travel_packages.components.agency.Agency;
import by.germanovich.travel_packages.components.customer.Customer;
import by.germanovich.travel_packages.components.order.Order;
import by.germanovich.travel_packages.components.tour.Tour;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Collection", propOrder = {
        "agency",
        "customer"
})
@XmlRootElement(name = "Collection", namespace = "http://www.travel-packages.com/Collection")

public class Collection implements Serializable {

    @XmlElement(required = true)
    private List<Agency> agency = new ArrayList<>();
    @XmlElement(required = true)
    private List<Customer> customer = new ArrayList<>();

//    private volatile static Collection uniqueInstance;

    public Collection() {
    }

//    public static Collection getInstance(){
//        if(uniqueInstance == null){
//            synchronized (Collection.class){
//                if(uniqueInstance == null){
//                    uniqueInstance = new Collection();
//                }
//            }
//        }
//        return uniqueInstance;
//    }

    @XmlElement
    public List<Agency> getAgency() {
        return agency;
    }

    public void addListAgency(List<Agency> agency) {
        this.agency.addAll(agency);
    }

    public void addAgency(Agency agency) {
        this.agency.add(agency);
    }

    @XmlElement
    public List<Customer> getCustomer() {
        return customer;
    }

    public void addListCustomer(List<Customer> customer) {
        this.customer.addAll(customer);
    }

    public void addCustomer(Customer customer) {
        this.customer.add(customer);
    }

    public void addTour(Tour tour, int numberAgency) {
        Agency agency = this.agency.get(numberAgency);
        agency.addTour(tour);
    }

    public void addOrder(Order order, int numberAgency) {
        Agency agency = this.agency.get(numberAgency);
        agency.addOrder(order);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Collection that = (Collection) o;
        return Objects.equals(agency, that.agency);
    }

    @Override
    public int hashCode() {
        return Objects.hash(agency);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Collection.class.getSimpleName() + "[", "]")
                .add("agency=" + agency)
                .add("customer=" + customer)
                .toString();
    }

    public void setAgency(List<Agency> agency) {
        this.agency = agency;
    }

    public void setCustomer(List<Customer> customer) {
        this.customer = customer;
    }
}
