package by.germanovich.travel_packages.utils;

public class Constants {

    public static final String THICK_LINE = "\n==================================================================";

    public static final String THIN_LINE = "------------------------------------------------------------------";

    public static final String INVALID_INPUT = "Неверный ввод. Попробуйте ещё раз.";

    public static final int EXIT = 0; // Key 0 from keyboard
    public static final int ADD_TEST_DATE = 1;
    public static final int ADD_TOUR = 2;
    public static final int ADD_ORDER = 3;
    public static final int ADD_AGENCY = 4;
    public static final int ADD_CUSTOMER = 5;
    public static final int REMOVE_AGENCY = 6;
    public static final int REMOVE_CUSTOMER = 7;
    public static final int REMOVE_ORDER = 8;
    public static final int REMOVE_TOUR = 9;
    public static final int SORT_BY_PRICE = 10;
    public static final int PRINT = 11;
    public static final int SAVE_DATA = 12;
    public static final int DOWNLOAD_DATA = 13;
}
