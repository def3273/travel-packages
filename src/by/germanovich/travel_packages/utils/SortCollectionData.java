package by.germanovich.travel_packages.utils;

import by.germanovich.travel_packages.components.agency.Agency;
import by.germanovich.travel_packages.components.tour.Tour;
import by.germanovich.travel_packages.enums.variety.Food;
import by.germanovich.travel_packages.enums.variety.Purpose;
import by.germanovich.travel_packages.enums.variety.Transport;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SortCollectionData {
    public static void sortByPrice(Collection collectionData) {

        for (int numberAgency = 0; numberAgency < collectionData.getAgency().size(); numberAgency++) {
            Collections.sort(collectionData.getAgency().get(numberAgency).getTour());
        }
    }

    public static List<Tour> getSort(
            Agency agency,
            Food food,
            Transport transport,
            int duration) {

        List<Tour> tours = new ArrayList<>();

        for (Tour tour : agency.getTour()) {
            if (tour.getFood() == food && tour.getTransport() == transport && tour.getDuration() == duration) {
                tours.add(tour);
            }
        }

        return tours;
    }

    public static List<Tour> getSort(
            Agency agency,
            Purpose purpose,
            Transport transport) {

        List<Tour> tours = new ArrayList<>();

        for (Tour tour : agency.getTour()) {
            if (tour.getPurpose() == purpose && tour.getTransport() == transport) {
                tours.add(tour);
            }
        }

        return tours;
    }

    public static List<Tour> getSort(
            Agency agency,
            Food food) {

        List<Tour> tours = new ArrayList<>();

        for (Tour tour : agency.getTour()) {
            if (tour.getFood() == food) {
                tours.add(tour);
            }
        }

        return tours;
    }
}
