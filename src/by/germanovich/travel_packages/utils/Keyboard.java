package by.germanovich.travel_packages.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Keyboard {

    private static Scanner input = new Scanner(System.in);
    private static BufferedReader read = new BufferedReader(new InputStreamReader(System.in));

    private static int getValue() {
        while (!input.hasNextInt()) {
            input.next();
            System.out.println("Enter again:");
        }

        return input.nextInt();
    }

    public static int inputNumberWithZero() {

        int value;

        while (true) {
            value = getValue();

            if (value >= 0) {
                break;
            } else {
                System.out.println("Error. Enter again: ");
            }
        }

        return value;
    }

    public static int inputNumber() {

        int value;

        while (true) {
            value = getValue();

            if (value > 0) {
                break;
            } else {
                System.out.println("Error. Enter again: ");
            }
        }

        return value;
    }

    public static String getName() {
        String name = null;
        try {
            name = read.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert name != null;
        if (name.isEmpty()) {
            System.out.println("Enter again:");
            try {
                name = read.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return name;
    }
}