package by.germanovich.travel_packages.enums.variety;

public enum Transport {
    CAR, BUS, PLANE
}
