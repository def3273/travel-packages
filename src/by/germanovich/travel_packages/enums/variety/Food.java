package by.germanovich.travel_packages.enums.variety;

public enum Food {
    BREAKFAST, DINNER, BRUNCH, ALL_INCLUSIVE
}
