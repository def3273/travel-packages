package by.germanovich.travel_packages.enums.menu_type;

import by.germanovich.travel_packages.utils.Keyboard;

import java.util.Arrays;

public enum CustomerType {
    BUSINESS,
    RESIDENTIAL;

    public static CommandType intToEnum() {
        System.out.println("Выбирете один из типов");
        System.out.println(Arrays.toString(CustomerType.values()));
        System.out.print("Введите цифру(1-" + CustomerType.values().length + "): ");
        int commandAction = Keyboard.inputNumber();

        switch (commandAction) {
            case 1: {
                return CommandType.CREATE_BUSINESS_CUSTOMER;
            }
            case 2: {
                return CommandType.CREATE_RESIDENTIAL_CUSTOMER;
            }
            default: {
                throw new IllegalArgumentException();
            }
        }
    }
}
