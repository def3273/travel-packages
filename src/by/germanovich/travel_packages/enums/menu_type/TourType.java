package by.germanovich.travel_packages.enums.menu_type;

import by.germanovich.travel_packages.utils.Keyboard;

import java.util.Arrays;

public enum TourType {
    ECONOMY,
    ELITE;

    public static CommandType intToEnum() {
        System.out.println("Выбирете один из типов");
        System.out.println(Arrays.toString(TourType.values()));
        System.out.print("Введите цифру(1-" + TourType.values().length + "): ");
        int commandAction = Keyboard.inputNumber();

        switch (commandAction) {
            case 1: {
                return CommandType.CREATE_ECONOMY_TOUR;
            }
            case 2: {
                return CommandType.CREATE_ELITE_TOUR;
            }
            default: {
                throw new IllegalArgumentException();
            }
        }
    }
}
